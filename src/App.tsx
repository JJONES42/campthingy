import React from 'react';
import {CssBaseline} from '@material-ui/core';
import TopAppBar from './TopAppBar';

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <TopAppBar />
    </React.Fragment>
  );
}

export default App;
