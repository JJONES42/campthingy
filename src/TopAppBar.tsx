import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import {Toolbar} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

export default function TopAppBar() {
  return (
    <AppBar>
      <Toolbar>
        <Typography variant="h6">
          CampThingy
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
